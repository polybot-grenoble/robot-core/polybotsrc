# PolybotSrc

Librairie utilisée sur les cartes STM de chaque robot.

## Pour ajouter le code en tant que dépendance d'un projet STM

```sh
git submodule add git@gitlab.com:polybot-grenoble/robot-core/polybotsrc.git PolybotSrc
```

## Pour initialiser le code ou le mettre à jour

```sh
git submodule update --init --recursive
```
