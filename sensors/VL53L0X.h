/**
* @file VL53L0X.h
* @authors https://github.com/pololu/vl53l0x-arduino/blob/master/VL53L0X.cpp
* @brief Library that interface a VL53L0X sensor.
* @version 0.1
* @date 10/10/2020
*
* Most of the functionality of this library is based on the VL53L0X API
* provided by ST (STSW-IMG005), and some of the explanatory comments are quoted
* or paraphrased from the API source code, API user manual (UM2039), and the
* VL53L0X datasheet.
*
* Refactored by Charles Blanchard to match the need and architecture of Polybot codes.
*
*/

#ifndef VL53L0X_H
#define VL53L0X_H

#include "main.h"
#include <stdio.h>

/**
* @struct VL53L0X_t
* @brief VL53L0X handler is used to represent a VL53L0X sensor and its configuration.
*
*/
typedef struct
{
    I2C_HandleTypeDef *i2c;					/**< I2C channel of the VL53L0X sensor. */
    uint8_t address; 						/**< I2C address of the VL53L0X sensor. */

    uint16_t io_timeout;					/**< I2C read timeout period. */
    uint8_t did_timeout;					/**< Indicate if a timeout occurred. */
    uint16_t timeout_start_ms;  			/**< Recorded time of timeout measurement. */

    uint8_t stop_variable; 					/**< Read by init and used when starting measurement; is StopVariable field of VL53L0X_DevData_t structure in API. */
    uint32_t measurement_timing_budget_us;	/**< Time allowed for one measurement. */

    uint8_t last_status;					/**< Status of last I2C transmission. */
} VL53L0X_t;

/**
* @enum vcselPeriodType
* @brief VCSEL (Vertical Cavity Surface Emitting Laser) period type
*/
typedef enum
{
	VcselPeriodPreRange, 	/**< Pre-range period. */
	VcselPeriodFinalRange	/**< Final-range period. */
} vcselPeriodType;


/**
*  Iniitializes and configures the sensor.
*  If the optional argument io_2v8 is 1, the sensor is configured for 2V8 mode (2.8 V I/O);
*  if 0, the sensor is left in 1V8 mode. Returns 1 if the initialization completed successfully.
* @param sensor The handler of the sensor.
* @param hi2c The I2C channel's handler on which the sensor is connected.
* @param io_2v8 Specify if the sensor is configured for 2V8 (0 for false, 1 for true)
* @return 1 in case of success, 0 otherwise.
*/
uint8_t VL53L0X_init(VL53L0X_t *sensor, I2C_HandleTypeDef *hi2c, uint8_t io_2v8);


/**
* Set the I2C address of a VL53L0X sensor
* @param sensor The handler of the sensor.
* @param new_addr The future I2C address of the sensor
*/
void VL53L0X_setAddress(VL53L0X_t *sensor, uint8_t new_addr);

uint8_t VL53L0X_askAddress(VL53L0X_t *sensor);

/**
* Get the I2C address of a VL53L0X sensor
* @param sensor The handler of the sensor.
* @return The address of the VL53L0X sensor
*/
uint8_t VL53L0X_getAddress(VL53L0X_t *sensor);


uint8_t VL53L0X_ping(VL53L0X_t *sensor);

/**
* Starts continuous ranging measurements.
* @param sensor The handler of the sensor.
* @param period_ms (0 for as often as possible)
*/
void VL53L0X_startContinuous(VL53L0X_t *sensor, uint32_t period_ms);

/**
* Stops continuous mode.
* @param sensor The handler of the sensor.
*/
void VL53L0X_stopContinuous(VL53L0X_t *sensor);


/**
* Returns a range reading in millimeters when continuous mode is active.
* @param sensor The handler of the sensor.
* @return The distance measured.
*/
uint16_t VL53L0X_readRangeContinuousMillimeters(VL53L0X_t *sensor);

/**
* Performs a single-shot ranging measurement and returns the reading in millimeters.
* @param sensor The handler of the sensor.
* @return The distance measured.
*/
uint16_t VL53L0X_readRangeSingleMillimeters(VL53L0X_t *sensor);


/**
* Sets a timeout period in milliseconds after which read operations will abort if the sensor is not ready.
* @param sensor The handler of the sensor.
* @param timeout The period (value of 0 will disable the timeout)
*/
void VL53L0X_setTimeout(VL53L0X_t *sensor, uint16_t timeout);

/**
* Returns the current timeout period setting.
* @param sensor The handler of the sensor.
* @return The current timeout period setting
*/
uint16_t VL53L0X_getTimeout(VL53L0X_t *sensor);

/**
* Indicates whether a read timeout has occurred since the last call to timeoutOccurred().
* @param sensor The handler of the sensor.
* @return 1 if timeout occured, 0 otherwise.
*/
uint8_t VL53L0X_timeoutOccurred(VL53L0X_t *sensor);

/**
* Sets the return signal rate limit to the given value
* This is the minimum amplitude of the signal reflected from the target and received by the sensor
* necessary for it to report a valid reading. Setting a lower limit increases the potential range
* of the sensor but also increases the likelihood of getting an inaccurate reading because of
* reflections from objects other than the intended target. This limit is initialized to 0.25 MCPS
* by default.
* @param sensor The handler of the sensor.
* @param limit_Mcps in units of MCPS (mega counts per second).
* @return The return value is a boolean indicating whether the requested limit was valid.
*/
uint8_t VL53L0X_setSignalRateLimit(VL53L0X_t *sensor, float limit_Mcps);

/**
* Gets the current signal rate limit in MCPS.
* @param sensor The handler of the sensor.
* @return The current signal rate limit in MCPS.
*/
float VL53L0X_getSignalRateLimit(VL53L0X_t *sensor);

/**
* Set the measurement timing budget in microseconds, which is the time allowed
* for one measurement; the ST API and this library take care of splitting the
* timing budget among the sub-steps in the ranging sequence. A longer timing
* budget allows for more accurate measurements. Increasing the budget by a
* factor of N decreases the range measurement standard deviation by a factor of
* sqrt(N). Defaults to about 33 milliseconds; the minimum is 20 ms.
* based on VL53L0X_set_measurement_timing_budget_micro_seconds()
*
* @param sensor The handler of the sensor.
* @param budget_us The measurement timing budget in microseconds.
*/
uint8_t VL53L0X_setMeasurementTimingBudget(VL53L0X_t *sensor, uint32_t budget_us);

/**
* Gets the current measurement timing budget
* @param sensor The handler of the sensor.
* @return The current measurement timing budget in microseconds.
*/
uint32_t VL53L0X_getMeasurementTimingBudget(VL53L0X_t *sensor);

/**
* Sets the VCSEL (vertical cavity surface emitting laser) pulse period for the given period type
* (VcselPeriodPreRange or VcselPeriodFinalRange) to the given value (in PCLKs).
* Longer periods increase the potential range of the sensor. Valid values are (even numbers only):
* Pre: 12 to 18 (initialized to 14 by default)
* Final: 8 to 14 (initialized to 10 by default)
* The return value is a boolean indicating whether the requested period was valid.
* @param sensor The handler of the sensor.
* @param type The period type for which the period need to be modified
* @param period_pclks The period to be set (in PCLKs)
*/
uint8_t VL53L0X_setVcselPulsePeriod(VL53L0X_t *sensor, vcselPeriodType type, uint8_t period_pclks);

/**
* Gets the current VCSEL pulse period for the given period type.
* @param sensor The handler of the sensor.
* @return The current VCSEL pulse period for the given period type.
*/
uint8_t VL53L0X_getVcselPulsePeriod(VL53L0X_t *sensor, vcselPeriodType type);


#endif /* __VL53L0X_H__ */
