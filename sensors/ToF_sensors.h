#ifndef TOF_SENSORS_H
#define TOF_SENSORS_H

#include <stdio.h>

#include "main.h"

//Number of ToF sensors (distance sensors)
#define NB_SENSORS_TOF 7
static_assert(NB_SENSORS_TOF <= 8, "multiplexer only supports up to 8 sensors");

/**
 * Initialize multiplexer and sensors
 * @param hi2c I2C handler
 */
void sensors_init(I2C_HandleTypeDef *hi2c);

/**
 * @param hi2c I2C handler
 * @param range distance in [mm]
 * @return a bitset of sensors, bit is 1 if sensor detected something in range
 * @return 0 if no obstacle detected
 */
uint8_t sensors_measurement(I2C_HandleTypeDef *hi2c, uint16_t range);

#endif
