// Tof Sensor
// https://www.st.com/en/imaging-and-photonics-solutions/vl53l0x.html
// https://www.st.com/resource/en/datasheet/vl53l0x.pdf

// Multiplexer
// https://www.dfrobot.com/product-1780.html
// https://drive.google.com/file/d/1TqQ5hjC7Qd4nFaqQuWEVhtHK0-eKoQxQ/view?usp=sharing

#include <stdio.h>
#include <stdlib.h>

#include "main.h"
#include "ToF_sensors.h"
#include "VL53L0X.h"

#define MULTIPLEXER_WRITE_ADDRESS 0xE0
#define MULTIPLEXER_READ_ADDRESS 0xE1

// Array of ToF sensors
VL53L0X_t sensor_ToF[NB_SENSORS_TOF];

/**
 * Select port from multiplexer.
 * @param hi2c I2C handler
 * @param i selected port
 * @return HAL_OK on success
 */
static HAL_StatusTypeDef select_multiplexer_port(I2C_HandleTypeDef *hi2c, int i) {
    if (i < 0 || i > 7) {
        return HAL_ERROR;
    }

    uint8_t TCA_data_write = 1 << i;
    return HAL_I2C_Master_Transmit(hi2c, MULTIPLEXER_WRITE_ADDRESS, &TCA_data_write, 1, 1000);
}

void sensors_init(I2C_HandleTypeDef *hi2c) {
    printf("sensors: start initialization\r\n");
    for (int i = 0; i < NB_SENSORS_TOF; i++) {
        HAL_StatusTypeDef s = select_multiplexer_port(hi2c, i);
        if (s != HAL_OK) {
            printf("select_multiplexer_port: status=%d, i=%d\r\n", s, i);
            continue; // Skip sensor if connection doesn't work
        }

        // Init sensor_ToF[i]
        VL53L0X_init(&sensor_ToF[i], hi2c, 0);

        // sensor_ToF[i] in "back-to-back" mode
        VL53L0X_startContinuous(&sensor_ToF[i], 0);
    }
    printf("sensors: end initialization\r\n");
}

uint8_t sensors_measurement(I2C_HandleTypeDef *hi2c, uint16_t range) {
    uint8_t detected = 0;
    for (int i = 0; i < NB_SENSORS_TOF; i++) {
        HAL_StatusTypeDef s = select_multiplexer_port(hi2c, i);
        if (s != HAL_OK) {
            printf("select_multiplexer_port: status=%d, i=%d\r\n", s, i);
            continue; // Skip sensor if connection doesn't work
        }

        // Read distance measurement from one sensor
        uint16_t distance = VL53L0X_readRangeContinuousMillimeters(&sensor_ToF[i]);

        // Verify if obstacle is present
        if (distance < range) {
            detected |= 1 << i;
            printf("sensor n°%d: d= %d mm\r\n", i, distance);
        }
    }
    return detected;
}
