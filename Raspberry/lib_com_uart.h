#ifndef LIB_COM_UART_H
#define LIB_COM_UART_H

#include "main.h"

#define SEND_SIZE 6
#define RECEIVE_SIZE 7

typedef enum {
    OK = 0, TIMEOUT = 1, UNKNOWN = 2, QUERY_ERROR = 3, CHKERR = 4, INTERNAL_ERROR = 5
} Status;

typedef enum {
    ORDER_COMMAND = 0, ORDER_READ = 1,
} Order;

typedef enum {
    CONNECTION_REQUEST = 0x00,
    LAUNCH_ROBOT = 0x01,
    DISTANCE = 0x02,
    ROTATION = 0x04,
    MOVE_STATE = 0x06,
    MOVE_VALUE = 0x07,
    MOVE_STOP = 0x08,
    OBSTACLE_TOF = 0x09,
    MOVE_RESTART = 0x10,
    BRAS_ALLUMER_POMPE = 0x11,
    BRAS_ETEINDRE_POMPE = 0x12,
    BRAS_TRANSPORT = 0x13,
    BRAS_GALERIE_HAUT = 0x14,
    BRAS_GALERIE_BAS = 0x15,
    BRAS_SOL = 0x16,
    BRAS_COTE_INCLINE = 0x17,
    BRAS_COTE_HORIZONTAL = 0x18,
    BRAS_REPLIE = 0x19,
    BRAS_DEPLOIE = 0x1A,
} Instruction;

typedef struct {
    uint32_t data;
    Order order;
    Instruction instruction;
    uint8_t checksum;
} Frame;

/**
 * Compute the checksum of an array
 * The checksum is the sum of the xor operator minus one
 * @param array is the message array
 * @param size is the size of the message
 * @return checksum of the array
 */
uint8_t checksum(uint8_t *array, uint8_t size);

/**
 * Send an answer to the Raspberry
 * @param status of the Raspberry's request
 * @param uint32_t value, integer to transmit
 */
void send_answer(UART_HandleTypeDef *huart, Status status, uint32_t value);

/**
 * @param buffer array containing the raw frame
 * @return a complete decoded frame containing every fields
 */
Frame decode_frame(uint8_t *buffer);

#endif
