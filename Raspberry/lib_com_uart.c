#include "main.h"
#include "lib_com_uart.h"

uint8_t tx_buff[SEND_SIZE] = { 0 };

uint8_t checksum(uint8_t *array, uint8_t size) {
    uint8_t chk = array[0];
    for (uint8_t i = 1; i < size - 1; i++) {
        chk ^= array[i];
    }
    return (chk - 1) % 256;
}

void send_answer(UART_HandleTypeDef *huart, Status status, uint32_t data) {
    uint8_t frame[SEND_SIZE] = { status, (data >> 24) & 0xFF, (data >> 16) & 0xFF, (data >> 8) & 0xFF, data & 0xFF, 0 };
    frame[SEND_SIZE - 1] = checksum(frame, SEND_SIZE);
    HAL_UART_Transmit(huart, frame, SEND_SIZE, 1000);
}

/**
 * @param buffer convert array of 4 uint8_t
 * @return `buffer` converted to uint32_t
 */
static uint32_t get_uint32(uint8_t *buffer) {
    return (buffer[0] << 24) + (buffer[1] << 16) + (buffer[2] << 8) + buffer[3];
}

Frame decode_frame(uint8_t *buffer) {
    Frame f;
    f.order = buffer[0];
    f.instruction = buffer[1];
    f.data = get_uint32(&buffer[2]);
    f.checksum = buffer[6];
    return f;
}
