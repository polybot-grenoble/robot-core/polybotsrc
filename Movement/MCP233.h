#ifndef MOVEMENT_MCP233_H
#define MOVEMENT_MCP233_H

#include "main.h"

typedef struct {
    UART_HandleTypeDef *huart;
    uint8_t id;
    uint32_t timeout;
    uint16_t crc;
} MCP233_t;

/**
 * Constructor, must be called once before any of the functions below
 */
HAL_StatusTypeDef MCP233_init(MCP233_t *mcp, uint8_t id, UART_HandleTypeDef *huart, uint32_t timeout);

/**
 * Control Motors speed (from 0 to 127)
 * speed = 127: full speed
 * speed = 64: half speed
 * speed = 0: full stop
 */
HAL_StatusTypeDef MCP233_driveForwardM1(MCP233_t *mcp, uint8_t speed);
HAL_StatusTypeDef MCP233_driveBackwardM1(MCP233_t *mcp, uint8_t speed);
HAL_StatusTypeDef MCP233_driveForwardM2(MCP233_t *mcp, uint8_t speed);
HAL_StatusTypeDef MCP233_driveBackwardM2(MCP233_t *mcp, uint8_t speed);
HAL_StatusTypeDef MCP233_driveForward(MCP233_t *mcp, uint8_t speed);
HAL_StatusTypeDef MCP233_driveBackward(MCP233_t *mcp, uint8_t speed);

/**
 * Fill `version` with the firmware version
 */
HAL_StatusTypeDef MCP233_readFirmwareVersion(MCP233_t *mcp, char version[48]);

/**
 * Return the encoder counts or the opposite if going backward
 */
HAL_StatusTypeDef MCP233_readEncoderCountM1(MCP233_t *mcp, int32_t *count);
HAL_StatusTypeDef MCP233_readEncoderCountM2(MCP233_t *mcp, int32_t *count);

/**
 * Return the speed or -speed if going backward
 * Returned value is in pulses per second
 */
HAL_StatusTypeDef MCP233_readEncoderSpeedM1(MCP233_t *mcp, int32_t *speed);
HAL_StatusTypeDef MCP233_readEncoderSpeedM2(MCP233_t *mcp, int32_t *speed);

/**
 * Reset counters to 0
 */
HAL_StatusTypeDef MCP233_resetEncoderCounts(MCP233_t *mcp);

/**
 * Set encoder count to `count`
 */
HAL_StatusTypeDef MCP233_setEncoderCountM1(MCP233_t *mcp, int32_t count);
HAL_StatusTypeDef MCP233_setEncoderCountM2(MCP233_t *mcp, int32_t count);

/**
 * Return the pulses counted in that last 300th of a second
 */
HAL_StatusTypeDef MCP233_readRawSpeedM1(MCP233_t *mcp, int32_t *speed);
HAL_StatusTypeDef MCP233_readRawSpeedM2(MCP233_t *mcp, int32_t *speed);

/**
 * Buffer: If a value of false is used the command will be buffered and executed
 * in the order sent. If a value of true is used the current running command is
 * stopped, any other commands in the buffer are deleted and the new command is executed.
 */

/**
 * Drive motor with a signed speed and distance value
 * The sign indicates which direction the motor will run
 */
HAL_StatusTypeDef MCP233_driveSpeedDistanceM1(MCP233_t *mcp, int32_t speed, uint32_t distance, uint8_t buffer);
HAL_StatusTypeDef MCP233_driveSpeedDistanceM2(MCP233_t *mcp, int32_t speed, uint32_t distance, uint8_t buffer);
HAL_StatusTypeDef MCP233_driveSpeedDistance(MCP233_t *mcp, int32_t speedM1, int32_t speedM2, uint32_t distance_M1,
        uint32_t distance_M2, uint8_t buffer);

/**
 * Drive motor with a speed, acceleration and distance value
 * The distance sign indicates which direction the motor will run
 */
HAL_StatusTypeDef MCP233_driveSpeedAccelDistanceM1(MCP233_t *mcp, uint32_t accel, uint32_t speed, int32_t distance,
        uint8_t buffer);
HAL_StatusTypeDef MCP233_driveSpeedAccelDistanceM2(MCP233_t *mcp, uint32_t accel, uint32_t speed, int32_t distance,
        uint8_t buffer);
HAL_StatusTypeDef MCP233_driveSpeedAccelDistance(MCP233_t *mcp, uint32_t accel, uint32_t speedM1, int32_t distance_M1,
        uint32_t speedM2, int32_t distance_M2, uint8_t buffer);

/**
 * Move M1 position from the current position to the specified new position and
 * hold the new position. Accel sets the acceleration value and deccel the
 * decceleration value. QSpeed sets the speed in quadrature pulses the motor
 * will run at after acceleration and before decceleration.
 */
HAL_StatusTypeDef MCP233_driveSpeedAccelDeccelPositionM1(MCP233_t *mcp, int32_t accel, int32_t speed, int32_t deccel,
        int32_t position, uint8_t buffer);
HAL_StatusTypeDef MCP233_driveSpeedAccelDeccelPositionM2(MCP233_t *mcp, int32_t accel, int32_t speed, int32_t deccel,
        int32_t position, uint8_t buffer);
HAL_StatusTypeDef MCP233_driveSpeedAccelDeccelPosition(MCP233_t *mcp, int32_t accel_M1, int32_t accel_M2, int32_t speedM1,
        int32_t speedM2, int32_t deccelM1, int32_t deccelM2, int32_t positionM1, int32_t position_M2, uint8_t buffer);

#endif /* MOVEMENT_MCP233_H */
