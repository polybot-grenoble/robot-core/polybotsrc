#ifndef ACTION_H
#define ACTION_H

#include <stdbool.h>

#include "main.h"
#include "MCP233.h"

/**
 * Physical parameters of the robot
 * wheel distance, individual wheel diameters, encoder ticks per rotation
 */
typedef struct {
    uint32_t whDist; //distance between encoder wheels in mm
    uint32_t dmWh1; //encoder 1 wheel diameter in mm
    uint32_t dmWh2; //encoder 2 wheel diameter in mm
    uint32_t ticks1; //encoder 1 ticks per rotation
    uint32_t ticks2; //encoder 2 ticks per rotation
} Move_geometry;

typedef struct {
    uint32_t acceleration;
    uint32_t deceleration;
    uint32_t speed;
    double precision;
} Move_settings;

typedef struct {
    const Move_geometry *g;
    const Move_settings *s;
    UART_HandleTypeDef *huart;
    int32_t startM1;
    int32_t startM2;
    int32_t targetM1;
    int32_t targetM2;
    MCP233_t mcp;
} Move;

/**
 * Initialize encoders MCP communication
 * @param huart the uart handler
 * @param g struct containing physical properties
 * @param s struct containing configuration properties
 * @return the struct containing all the required fields
 */
Move move_init(UART_HandleTypeDef *huart, const Move_geometry *g, const Move_settings *s);


/**
 * Move the robot forward or backward
 * @param m the struct created by `move_init()`
 * @param dist distance in [mm]
 * @return HAL_OK on success
 */
HAL_StatusTypeDef move_straight(Move *m, int32_t dist);

/**
 * Move the robot forward
 * @param m the struct created by `move_init()`
 * @param dist distance in [mm]
 * @return HAL_OK on success
 */
HAL_StatusTypeDef move_forward(Move *m, int32_t dist);

/**
 * Move the robot backward
 * @param m the struct created by `move_init()`
 * @param dist distance in [mm]
 * @return HAL_OK on success
 */
HAL_StatusTypeDef move_backward(Move *m, int32_t dist);

/**
 * Rotate the robot
 * @param m the struct created by `move_init()`
 * @param angle in [rad], should be negative for anti-clockwise rotation
 * @return HAL_OK on success
 */
HAL_StatusTypeDef move_rotation(Move *m, int32_t angle);

/**
 * @param m the struct created by `move_init()`
 * @return true if last action is done
 */
bool is_move_finished(Move *m);

/**
 * Read encoders value
 * @param m the struct created by `move_init()`
 * @param e1 output parameter for encoder 1
 * @param e2 output parameter for encoder 2
 */
void move_read_encoders(Move *m, int32_t *e1, int32_t *e2);

/**
 * @param m the struct created by `move_init()`
 * @return the currently achieved distance in [mm]
 */
int32_t get_current_distance(Move *m);

/**
 * @param m the struct created by `move_init()`
 * @return the currently achieved rotation in [deg]
 */
int32_t get_current_rotation(Move *m);

/**
 * Stop the robot
 * @param m the struct created by `move_init()`
 * @return HAL_OK on success
 */
HAL_StatusTypeDef move_stop(Move *m);

#endif /* ACTION_H */
