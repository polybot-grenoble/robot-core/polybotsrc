#include <math.h>
#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>

#include "action.h"

Move move_init(UART_HandleTypeDef *huart, const Move_geometry *g, const Move_settings *s) {
    Move m = { .g = g, .s = s, .huart = huart, .startM1 = 0, .startM2 = 0, .targetM1 = 0, .targetM2 = 0, };

    MCP233_init(&m.mcp, 128, m.huart, 1000);
    move_stop(&m);
    MCP233_resetEncoderCounts(&m.mcp);
    return m;
}

static void update_target(Move *m, int32_t targetM1, int32_t targetM2) {
    m->targetM1 += targetM1;
    m->targetM2 += targetM2;
    /*if (targetM1 > 0 && targetM2 > 0) {
     int32_t min = abs(targetM1 < targetM2) ? targetM1 : targetM2;
     m->targetM1 -= min;
     m->targetM2 -= min;
     return;
     }
     if (targetM1 < 0 && targetM2 < 0) {

     }*/
}

HAL_StatusTypeDef move_straight(Move *m, int32_t dist) {
    //dist in [mm]
    int32_t targetM1 = dist / ((M_PI * m->g->dmWh1) / m->g->ticks1);
    int32_t targetM2 = dist / ((M_PI * m->g->dmWh2) / m->g->ticks2);

    update_target(m, targetM1, targetM2);
    printf("send command forward/backward, encoders target: M1=%ld, M2=%ld\r\n", m->targetM1, m->targetM2);

    return MCP233_driveSpeedAccelDeccelPosition(&m->mcp, m->s->acceleration, m->s->acceleration, m->s->speed, m->s->speed,
            m->s->deceleration, m->s->deceleration, m->targetM1, m->targetM2, 1);
}

HAL_StatusTypeDef move_forward(Move *m, int32_t dist) {
    return move_straight(m, dist);
}

HAL_StatusTypeDef move_backward(Move *m, int32_t dist) {
    return move_straight(m, -dist);
}

HAL_StatusTypeDef move_rotation(Move *m, int32_t angle) {
    double dist = -(angle) * M_PI * m->g->whDist / 360.0; //arc length to be traveled by each wheel
    int32_t targetM1 = dist / ((M_PI * m->g->dmWh1) / m->g->ticks1);
    int32_t targetM2 = -dist / ((M_PI * m->g->dmWh2) / m->g->ticks2);

    update_target(m, targetM1, targetM2);
    printf("send command rotation, encoders target: M1=%ld, M2=%ld\r\n", m->targetM1, m->targetM2);

    return MCP233_driveSpeedAccelDeccelPosition(&m->mcp, m->s->acceleration, m->s->acceleration, m->s->speed, m->s->speed,
            m->s->deceleration, m->s->deceleration, m->targetM1, m->targetM2, 1);
}

bool is_move_finished(Move *m) {
    int32_t m1_low = m->targetM1 - m->targetM1 * m->s->precision;
    int32_t m1_high = m->targetM1 + m->targetM1 * m->s->precision;
    int32_t m2_low = m->targetM2 - m->targetM2 * m->s->precision;
    int32_t m2_high = m->targetM2 + m->targetM2 * m->s->precision;

    int32_t e1, e2;
    move_read_encoders(m, &e1, &e2);

    printf("value enc: M1=%ld, M2=%ld\r\n", e1, e2);
    printf("%ld < (M1) < %ld \n\r  %ld < (M2) < %ld\r\n", m1_low, m1_high, m2_low, m2_high);

    // Swap if < 0
    int32_t tmp;
    if (m1_low > m1_high) {
        tmp = m1_low;
        m1_low = m1_high;
        m1_high = tmp;
    }
    if (m2_low > m2_high) {
        tmp = m2_low;
        m2_low = m2_high;
        m2_high = tmp;
    }

    return e1 >= m1_low && e1 <= m1_high && e2 >= m2_low && e2 <= m2_high;
}

void move_read_encoders(Move *m, int32_t *e1, int32_t *e2) {
    if (e1 != NULL) {
        MCP233_readEncoderCountM1(&m->mcp, e1);
    }
    if (e2 != NULL) {
        MCP233_readEncoderCountM2(&m->mcp, e2);
    }
}

int32_t get_current_distance(Move *m) {
    int32_t e1;
    int32_t e2;
    move_read_encoders(m, &e1, &e2);

    int32_t distance_done = (((e1 - m->startM1) * ((M_PI * m->g->dmWh1) / m->g->ticks1)));

    printf("distance réalisé avant stop: %ld mm\r\n", distance_done);
    return distance_done;
}

int32_t get_current_rotation(Move *m) {
    int32_t e1;
    int32_t e2;
    move_read_encoders(m, &e1, &e2);

    int32_t angle_done = (((e1 - m->startM1) * 360 * (M_PI * m->g->dmWh1 / m->g->ticks1)) / (M_PI * m->g->whDist));

    printf("angle réalisé avant stop: %ld °\r\n", angle_done);
    return angle_done;
}

HAL_StatusTypeDef move_stop(Move *m) {
    return MCP233_driveForward(&m->mcp, 0);
}
