// https://www.basicmicro.com/MCP233-Dual-30A-34VDC-Advanced-Motor-Controller_p_39.html
// https://downloads.basicmicro.com/docs/mcp_user_manual.pdf

#include <stdarg.h>

#include "MCP233.h"

#define SetDWORDval( arg )    (uint8_t)((arg)>>24),(uint8_t)((arg)>>16),(uint8_t)((arg)>>8),(uint8_t)(arg)
#define SetWORDval( arg )     (uint8_t)((arg)>>8),(uint8_t)(arg)
#define GetDWORDval( buffer ) ((*(buffer)<<24) + (*(buffer+1)<<16) + (*(buffer+2)<<8) + *(buffer+3))
#define GetWORDval( buffer )  (*(buffer+2)<<8 + *(buffer+3))

HAL_StatusTypeDef MCP233_init(MCP233_t *mcp, uint8_t id, UART_HandleTypeDef *huart, uint32_t timeout) {
    mcp->huart = huart;
    mcp->id = id;
    mcp->timeout = timeout;

    //Reset Encoders
    MCP233_resetEncoderCounts(mcp);

    return HAL_OK;
}

// Méthodes privée pour crc
static void crcClear(MCP233_t *mcp) {
    mcp->crc = 0;
}

static void crcUpdate(MCP233_t *mcp, uint8_t data) {
    mcp->crc = mcp->crc ^ ((uint16_t) data << 8);
    for (int i = 0; i < 8; i++) {
        if (mcp->crc & 0x8000)
            mcp->crc = (mcp->crc << 1) ^ 0x1021;
        else
            mcp->crc <<= 1;
    }
}

static uint16_t crcGet(MCP233_t *mcp) {
    return mcp->crc;
}

/**
 * Send array `data` of size `n` to UART
 */
static uint8_t write_n(MCP233_t *mcp, uint8_t cnt, ...) {
    uint8_t status = 0;
    uint8_t answer = 0;

    //clear crc
    crcClear(mcp);

    //send address and update crc
    status |= HAL_UART_Transmit(mcp->huart, &mcp->id, 1, mcp->timeout);
    crcUpdate(mcp, mcp->id);

    //send data and update crc
    va_list marker;
    va_start(marker, cnt); /* Initialize variable arguments. */
    for (uint8_t index = 0; index < cnt; index++) {
        uint8_t data = va_arg(marker, int);
        status |= HAL_UART_Transmit(mcp->huart, &data, 1, mcp->timeout);
        crcUpdate(mcp, data);
    }
    va_end(marker); /* Reset variable arguments.      */

    //get crc and send it
    uint8_t crc[2] = { SetWORDval(crcGet(mcp)) };
    status |= HAL_UART_Transmit(mcp->huart, crc, 2, mcp->timeout);

    //wait for answer
    status |= HAL_UART_Receive(mcp->huart, &answer, 1, mcp->timeout);

    //control status
    if (status != HAL_OK) {
        return status;
    }

    //control answer
    if (answer != 0xFF) {
        return HAL_ERROR;
    }

    return HAL_OK;
}

static HAL_StatusTypeDef read_n(MCP233_t *mcp, uint8_t cmd, uint8_t *buffer, uint8_t cnt) {
    uint8_t crc[2];
    HAL_StatusTypeDef status = 0;

    //clear crc
    crcClear(mcp);

    //send address and update crc
    status |= HAL_UART_Transmit(mcp->huart, &mcp->id, 1, mcp->timeout);
    crcUpdate(mcp, mcp->id);

    //send cmd and update crc
    status |= HAL_UART_Transmit(mcp->huart, &cmd, 1, mcp->timeout);
    crcUpdate(mcp, cmd);

    //receive data and crc
    status |= HAL_UART_Receive(mcp->huart, buffer, cnt, mcp->timeout);
    status |= HAL_UART_Receive(mcp->huart, crc, 2, mcp->timeout);

    //compute crc on received data
    for (int i = 0; i < cnt; i++)
        crcUpdate(mcp, buffer[i]);

    //control status
    if (status != HAL_OK) {
        return status;
    }

    //check received crc with computed crc
    if (crcGet(mcp) != ((crc[0] << 8) + crc[1])) {
        return HAL_ERROR;
    }

    return HAL_OK;
}

static HAL_StatusTypeDef read_4_1(MCP233_t *mcp, uint8_t cmd, uint32_t *arg1, uint8_t *arg2) {
    uint8_t buffer[5] = { 0, 0, 0, 0, 0 };

    uint8_t ret = read_n(mcp, cmd, buffer, 5);

    *arg1 = GetDWORDval(buffer);
    *arg2 = buffer[5];

    return ret;
}

// 0-7 Compatibility commands
#define DRIVE_FORWARD_M1  0
#define DRIVE_BACKWARD_M1 1
#define DRIVE_FORWARD_M2  4
#define DRIVE_BACKWARD_M2 5

// 8-13 Mixed mode compatibility commands
#define DRIVE_FORWARD  8
#define DRIVE_BACKWARD 9

// Advance Packet Serial Mode
#define READ_FIRMWARE_VERSION 21

// Encoder Commands
#define READ_ENCODER_COUNT_M1 16
#define READ_ENCODER_COUNT_M2 17
#define READ_ENCODER_SPEED_M1 18
#define READ_ENCODER_SPEED_M2 19
#define RESET_ENCODER_COUNTS  20
#define SET_ENCODER_COUNT_M1  22
#define SET_ENCODER_COUNT_M2  23
#define READ_RAW_SPEED_M1     30
#define READ_RAW_SPEED_M2     31
#define READ_ENCODEURS_COUNTS 78
#define READ_MOTOR_SPEEDS     79

// Advanced motor control
#define DRIVE_SPEED_DISTANCE_M1              41
#define DRIVE_SPEED_DISTANCE_M2              42
#define DRIVE_SPEED_DISTANCE                 43
#define DRIVE_SPEED_ACCEL_DISTANCE_M1        44
#define DRIVE_SPEED_ACCEL_DISTANCE_M2        45
#define DRIVE_SPEED_ACCEL_DISTANCE           46
#define DRIVE_SPEED_ACCEL_DECCEL_POSITION_M1 65
#define DRIVE_SPEED_ACCEL_DECCEL_POSITION_M2 66
#define DRIVE_SPEED_ACCEL_DECCEL_POSITION    67

HAL_StatusTypeDef MCP233_driveForwardM1(MCP233_t *mcp, uint8_t speed) {
    return write_n(mcp, 2, DRIVE_FORWARD_M1, speed);
}

HAL_StatusTypeDef MCP233_driveBackwardM1(MCP233_t *mcp, uint8_t speed) {
    return write_n(mcp, 2, DRIVE_BACKWARD_M1, speed);
}

HAL_StatusTypeDef MCP233_driveForwardM2(MCP233_t *mcp, uint8_t speed) {
    return write_n(mcp, 2, DRIVE_FORWARD_M2, speed);
}

HAL_StatusTypeDef MCP233_driveBackwardM2(MCP233_t *mcp, uint8_t speed) {
    return write_n(mcp, 2, DRIVE_BACKWARD_M2, speed);
}

HAL_StatusTypeDef MCP233_driveForward(MCP233_t *mcp, uint8_t speed) {
    return write_n(mcp, 2, DRIVE_FORWARD, speed);
}

HAL_StatusTypeDef MCP233_driveBackward(MCP233_t *mcp, uint8_t speed) {
    return write_n(mcp, 2, DRIVE_BACKWARD, speed);
}

HAL_StatusTypeDef MCP233_readFirmwareVersion(MCP233_t *mcp, char version[48]) {
    uint8_t i = 0;
    uint8_t status = 0;
    uint8_t cmd = READ_FIRMWARE_VERSION;
    uint8_t crc[2];

    //clear crc
    crcClear(mcp);

    //send id and update crc
    status |= HAL_UART_Transmit(mcp->huart, &mcp->id, 1, mcp->timeout);
    crcUpdate(mcp, mcp->id);

    //send cmd and update crc
    status |= HAL_UART_Transmit(mcp->huart, &cmd, 1, mcp->timeout);
    crcUpdate(mcp, cmd);

    //check that id and cmd are sent
    if (status != 0) {
        return status;
    }

    //receive data
    for (i = 0; i < 48; i++) {
        status |= HAL_UART_Receive(mcp->huart, (uint8_t*) &version[i], 1, mcp->timeout);
        crcUpdate(mcp, version[i]);

        if (status != 0) {
            return status;
        }

        if (version[i] == '\0') {
            //receive crc
            status |= HAL_UART_Receive(mcp->huart, crc, 2, mcp->timeout);

            //check received crc with computed crc
            if (crcGet(mcp) == ((crc[0] << 8) + crc[1])) {
                return HAL_OK;
            } else {
                return HAL_ERROR;
            }
        }
    }
    return HAL_ERROR;
}

/**
 * Read speed, status and crc
 * Check for underflow and overflow
 * Return the encoder counts or the opposite if going backward
 */

HAL_StatusTypeDef MCP233_readEncoderCountM1(MCP233_t *mcp, int32_t *count) {
    uint32_t receivedCount;
    uint8_t status;

    uint8_t ret = read_4_1(mcp, READ_ENCODER_COUNT_M1, &receivedCount, &status);

    *count = ((status & 0x1) == 1) ? (int32_t) receivedCount : (int32_t) receivedCount;

    return ret;
}

HAL_StatusTypeDef MCP233_readEncoderCountM2(MCP233_t *mcp, int32_t *count) {
    uint32_t receivedCount;
    uint8_t status;

    uint8_t ret = read_4_1(mcp, READ_ENCODER_COUNT_M2, &receivedCount, &status);

    *count = ((status & 0x1) == 1) ? (int32_t) receivedCount : (int32_t) receivedCount;

    return ret;
}

HAL_StatusTypeDef MCP233_readEncoderSpeedM1(MCP233_t *mcp, int32_t *speed) {
    uint32_t receivedSpeed;
    uint8_t status;

    uint8_t ret = read_4_1(mcp, READ_ENCODER_SPEED_M1, &receivedSpeed, &status);

    *speed = ((status) == 1) ? -(int32_t) receivedSpeed : (int32_t) receivedSpeed;

    return ret;
}

HAL_StatusTypeDef MCP233_readEncoderSpeedM2(MCP233_t *mcp, int32_t *speed) {
    uint32_t receivedSpeed = 0;
    uint8_t status = 0;

    uint8_t ret = read_4_1(mcp, READ_ENCODER_SPEED_M2, &receivedSpeed, &status);

    *speed = ((status) == 1) ? -(int32_t) receivedSpeed : (int32_t) receivedSpeed;

    return ret;
}

HAL_StatusTypeDef MCP233_resetEncoderCounts(MCP233_t *mcp) {
    uint8_t ret = write_n(mcp, 1, 20);
    return ret;
}

HAL_StatusTypeDef MCP233_setEncoderCountM1(MCP233_t *mcp, int32_t count) {
    return write_n(mcp, 5, SET_ENCODER_COUNT_M1, SetDWORDval(count));
}

HAL_StatusTypeDef MCP233_setEncoderCountM2(MCP233_t *mcp, int32_t count) {
    return write_n(mcp, 5, SET_ENCODER_COUNT_M2, SetDWORDval(count));
}

HAL_StatusTypeDef MCP233_readRawSpeedM1(MCP233_t *mcp, int32_t *speed) {
    uint32_t receivedSpeed = 0;
    uint8_t status = 0;

    uint8_t ret = read_4_1(mcp, READ_RAW_SPEED_M1, &receivedSpeed, &status);

    *speed = ((status) == 1) ? -(int32_t) receivedSpeed : (int32_t) receivedSpeed;

    return ret;
}

HAL_StatusTypeDef MCP233_readRawSpeedM2(MCP233_t *mcp, int32_t *speed) {
    uint32_t receivedSpeed = 0;
    uint8_t status = 0;

    uint8_t ret = read_4_1(mcp, READ_RAW_SPEED_M2, &receivedSpeed, &status);

    *speed = ((status) == 1) ? -(int32_t) receivedSpeed : (int32_t) receivedSpeed;

    return ret;
}

HAL_StatusTypeDef MCP233_driveSpeedDistanceM1(MCP233_t *mcp, int32_t speed, uint32_t distance, uint8_t buffer) {
    return write_n(mcp, 10,
    DRIVE_SPEED_DISTANCE_M1, SetDWORDval(speed), SetDWORDval(distance), buffer);
}

HAL_StatusTypeDef MCP233_driveSpeedDistanceM2(MCP233_t *mcp, int32_t speed, uint32_t distance, uint8_t buffer) {
    return write_n(mcp, 10,
    DRIVE_SPEED_DISTANCE_M2, SetDWORDval(speed), SetDWORDval(distance), buffer);
}

HAL_StatusTypeDef MCP233_driveSpeedDistance(MCP233_t *mcp, int32_t speed_M1, int32_t speed_M2, uint32_t distance_M1,
        uint32_t distance_M2, uint8_t buffer) {
    return write_n(mcp, 18,
    DRIVE_SPEED_DISTANCE_M1, SetDWORDval(speed_M1), SetDWORDval(distance_M1), SetDWORDval(speed_M2),
            SetDWORDval(distance_M2), buffer);
}

HAL_StatusTypeDef MCP233_driveSpeedAccelDistanceM1(MCP233_t *mcp, uint32_t accel, uint32_t speed, int32_t distance,
        uint8_t buffer) {
    return write_n(mcp, 14,
    DRIVE_SPEED_ACCEL_DISTANCE_M1, SetDWORDval(accel), SetDWORDval(speed), SetDWORDval(distance), buffer);
}

HAL_StatusTypeDef MCP233_driveSpeedAccelDistanceM2(MCP233_t *mcp, uint32_t accel, uint32_t speed, int32_t distance,
        uint8_t buffer) {
    return write_n(mcp, 14,
    DRIVE_SPEED_ACCEL_DISTANCE_M2, SetDWORDval(accel), SetDWORDval(speed), SetDWORDval(distance), buffer);
}

HAL_StatusTypeDef MCP233_driveSpeedAccelDistance(MCP233_t *mcp, uint32_t accel, uint32_t speed_M1, int32_t distance_M1,
        uint32_t speed_M2, int32_t distance_M2, uint8_t buffer) {
    return write_n(mcp, 22,
    DRIVE_SPEED_ACCEL_DISTANCE, SetDWORDval(accel), SetDWORDval(speed_M1), SetDWORDval(distance_M1),
            SetDWORDval(speed_M2), SetDWORDval(distance_M2), buffer);
}

HAL_StatusTypeDef MCP233_driveSpeedAccelDeccelPositionM1(MCP233_t *mcp, int32_t accel, int32_t speed, int32_t deccel,
        int32_t position, uint8_t buffer) {
    return write_n(mcp, 18,
    DRIVE_SPEED_ACCEL_DECCEL_POSITION_M1, SetDWORDval(accel), SetDWORDval(speed), SetDWORDval(deccel),
            SetDWORDval(position), buffer);
}

HAL_StatusTypeDef MCP233_driveSpeedAccelDeccelPositionM2(MCP233_t *mcp, int32_t accel, int32_t speed, int32_t deccel,
        int32_t position, uint8_t buffer) {
    return write_n(mcp, 18,
    DRIVE_SPEED_ACCEL_DECCEL_POSITION_M2, SetDWORDval(accel), SetDWORDval(speed), SetDWORDval(deccel),
            SetDWORDval(position), buffer);
}

HAL_StatusTypeDef MCP233_driveSpeedAccelDeccelPosition(MCP233_t *mcp, int32_t accel_M1, int32_t accel_M2, int32_t speed_M1,
        int32_t speed_M2, int32_t deccel_M1, int32_t deccel_M2, int32_t position_M1, int32_t position_M2,
        uint8_t buffer) {
    return write_n(mcp, 34,
    DRIVE_SPEED_ACCEL_DECCEL_POSITION, SetDWORDval(accel_M1), SetDWORDval(speed_M1), SetDWORDval(deccel_M1),
            SetDWORDval(position_M1), SetDWORDval(accel_M2), SetDWORDval(speed_M2), SetDWORDval(deccel_M2),
            SetDWORDval(position_M2), buffer);
}
