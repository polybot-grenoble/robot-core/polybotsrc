#include <stdio.h>
#include "ventouse.h"
#include "../../Core/Inc/main.h"
#include "../../Drivers/STM32F4xx_HAL_Driver/Inc/stm32f4xx_hal_gpio.h"

void move_bras(const PositionBrasI positionBrasI) {
    int bits = (int) positionBrasI;
    printf("Position = %d, bits = %01X%01X%01X\r\n", positionBrasI, (bits>>2)&1, (bits>>1)&1, (bits>>0)&1);
    HAL_GPIO_WritePin(GPIOB, bras_1_Pin, !!(bits&1));
    HAL_GPIO_WritePin(GPIOB, bras_2_Pin, !!(bits&2));
    HAL_GPIO_WritePin(GPIOB, bras_3_Pin, !!(bits&4));

    /*RCC_AHB1ENR |= 2;
    GPIOB->MODER &= ~(1<<31 + 1<<29 + 1<<27);
    GPIOB->MODER |= ~(1<<30 + 1<<28 + 1<<26);*/
    GPIOB->ODR &= ~((1<<15) + (1<<14) + (1<<13));
    GPIOB->ODR |= bits<<13;
}

void pump_onOff(const int onOff) {
    printf("Pump %s\r\n", onOff ? "on" : "off");
    HAL_GPIO_WritePin(GPIOB, pump_Pin, onOff);
}
