#ifndef VENTOUSE
#define VENTOUSE

typedef enum {TRANSPORT, GALERIE_HAUT, GALERIE_BAS, SOL, COTE_INCLINE, COTE_HORIZONTAL, REPLIE, DEPLOIE} PositionBrasI;

void move_bras(const PositionBrasI);

void pump_onOff(const int);

#endif
