#include "servo.h"

void servo_open(TIM_HandleTypeDef htim) {
    //Open the gate (put +/- 90° to each servo)
    htim.Instance->CCR1 = 25;   //25+0.40*0;
    htim.Instance->CCR2 = 103;  //25+0.40*180;

    //ATTENTION : Channel must be modified if you change the configuration
}

void servo_close(TIM_HandleTypeDef htim) {
    //Close the gate (put +/- 90° to each servo)
    htim.Instance->CCR1 = 64;   //25+0.40*90;
    htim.Instance->CCR2 = 64;   //25+0.40*90;

    //ATTENTION : Channel must be modified if you change the configuration
}

void servo_open_flag(TIM_HandleTypeDef htim) {
    //Open the gate (put +/- 90° to each servo)
    htim.Instance->CCR1 = 2;    //25+0.40*0;

    //ATTENTION : Channel must be modified if you change the configuration
}

void servo_setangle(uint16_t value, TIM_HandleTypeDef htim, uint8_t channel) {
    //Timer x Channel 1
    if (channel == 1)
        htim.Instance->CCR1 = 25 + 0.40 * value;

    //Timer x Channel 1
    if (channel == 2)
        htim.Instance->CCR2 = 25 + 0.40 * value;

    //Timer x Channel 3
    if (channel == 3)
        htim.Instance->CCR3 = 25 + 0.40 * value;

    HAL_Delay(500);
}
