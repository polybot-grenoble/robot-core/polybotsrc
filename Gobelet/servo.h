#ifndef GOBELET_SERVO_H
#define GOBELET_SERVO_H

#include "main.h"

void servo_setangle(uint16_t value, TIM_HandleTypeDef htim, uint8_t channel);
void servo_open(TIM_HandleTypeDef htim);
void servo_close(TIM_HandleTypeDef htim);
void servo_open_flag(TIM_HandleTypeDef htim);

#endif /* GOBELET_SERVO_H */
